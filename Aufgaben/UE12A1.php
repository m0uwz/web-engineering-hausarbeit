<!doctype html>
<h1>Registrierung</h1>
<form method="post">
    <fieldset>
        <legend><b>Melde dich an!</b></legend>
        <br>
        Name:<br>
        <input type="text" name="username">
        <br>
        Passwort:<br>
        <input type="text" name="password">
        <br><br><br>
        <input type="submit" value="Registrieren">
    </fieldset>
</form>
</html>

<?php
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $file = 'data/raw_passwd.csv';
        $new_line = md5($username) . ',' . md5($password) . "\n";

        if (file_put_contents($file, $new_line, FILE_APPEND|LOCK_EX)) {
            echo "<script>alert('Vielen Dank für deine Registrierung!')</script>";
        }
    }


function debug_to_console($data, $context = 'Debug in Console') {

    // Buffering to solve problems frameworks, like header() in this and not a solid return.
    ob_start();

    $output  = 'console.info(\'' . $context . ':\');';
    $output .= 'console.log(' . json_encode($data) . ');';
    $output  = sprintf('<script>%s</script>', $output);

    echo $output;
}
?>
/* Task 1 */

function identify_function(arg) {
    return function() {
        return arg;
    }
}

const idf = identify_function("Hello World :-)");
console.log(idf());


/* Task 2 */

function addf(x) {
    return function(y) {
        return x + y;
    }
}

console.log(addf(3)(4));


/* Task 3 */

function add(x, y) {
    return x + y;
}

function mul(x, y) {
    return x * y;
}

function applyf(fun) {
    return function(x) {
        return function(y) {
            return fun(x, y);
        }
    };
}

var addf = applyf(add);
console.log(addf(3)(4));
console.log(applyf(add)(3)(4));
console.log(applyf(mul)(2)(4));


/* Task 4 */

function curry(fun, x) {
    return function(y) {
        return fun(x, y);
    }
}

const add3 = curry(add, 3);
console.log(add3(6));
console.log(curry(mul, 5)(2));
console.log(curry(add, 7)(4));


/* Task 5 */

const incOne = addf(1);
const incTwo = applyf(add)(1);
const incThree = curry(add, 1);

console.log(incOne(12));
console.log(incTwo(12));
console.log(incThree(12));
console.log(incOne(incTwo(incThree(12))));


/* Task 6 */

function methodize(fun) {
    return function(y) {
        return fun(this, y);
    }
}

Number.prototype.add = methodize(add);
console.log((3).add(4));


/* Task 7 */

function demethodize(fun) {
    return function (x, y) {
        return fun.call(x, y);
    }
}

console.log(demethodize(Number.prototype.add)(5,6));


/* Task 8 */

function twice(fun) {
    return function (x) {
        return fun(x, x);
    }
}

const double = twice(add);
console.log(double(11));
const square = twice(mul);
console.log(square(11));


/* Task 9 */

function composeu(fun1, fun2) {
    return function (x) {
        return fun2(fun1(x));
    }
}

console.log(composeu(double, square)(3));


/* Task 10 */

function composeb(fun1, fun2) {
    return function (x, y, z) {
        return fun2(fun1(x, y), z);
    }
}

console.log(composeb(add, mul)(2, 3, 5));


/* Task 11 */

function once(fun) {
    var called = false;
        return function (x, y) {
            if (!called) {
                called = true;
                return fun(x, y);
            }
            else {
                console.log("Function was already called. Can be executed only once.");
            }
        }
}

const add_once = once(add);
console.log(add_once(3, 4));
add_once(3, 4);


/* Task 12 */

function counterf(x) {
    let num = x;
    return {
        inc: function() {
            return ++x;
        },
        dec: function() {
            return --x;
        }
    }
}

const counter = counterf(10);
console.log(counter.inc());
console.log(counter.dec());


/* Task 13 */

function revocable(fun) {
    let func = fun;
    return {
        invoke: function(x) {
            return func(x);
        },
        revoke: function() {
            func = null;
        }
    }
}

var temp = revocable(console.log);
temp.invoke(7);
temp.revoke();
/* temp.invoke(8); */


/* Task 14 */

function arrayWrapper() {
    let _array = [];
    return {
        get: function(i) {
            return _array[i];
        },
        store: function(item) {
            _array.unshift(item);
        },
        append: function(item) {
            _array.push(item);
        }
    }
}

const rapper = arrayWrapper();
rapper.append("Chewie");
rapper.append("Schmu");
console.log(rapper.get(0));
console.log(rapper.get(1));

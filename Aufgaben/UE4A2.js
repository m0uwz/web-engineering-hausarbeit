/* Task 1 */

function makeQueue() {
    let queue = [];
    return {
        getElement: function() {
            return queue.shift();
        },
        insertElement: function(e) {
            queue.push(e);
        },
        getQueue: function() {
            return queue;
        }
    }
}

let myQ = makeQueue();
myQ.insertElement("a");
myQ.insertElement("b");
myQ.insertElement("c");
console.log(myQ.getQueue());
console.log(myQ.getElement());
console.log(myQ.getQueue());
myQ.insertElement("d");
console.log(myQ.getQueue());
console.log(myQ.getElement());
console.log(myQ.getQueue());


/* Task 2 */

function makeSet() {
    let set = [];
    return {
        getElement: function() {
            return set[Math.floor(Math.random() * set.length)];
        },
        insertElement: function(e) {
            if (!(set.includes(e))) set.push(e);
        },
        deleteElement: function(e) {
            index = set.indexOf(e);
            if (index >= 0) set.splice(index, 1);
        },
        getSet: function() {
            return set;
        }
    }
}

let mySet = makeSet();
mySet.insertElement("a");
mySet.insertElement("b");
mySet.insertElement("c");
console.log(mySet.getSet());
mySet.insertElement("c");
console.log(mySet.getSet());
mySet.deleteElement("d");
console.log(mySet.getSet());
mySet.deleteElement("a");
console.log(mySet.getSet());
console.log(mySet.getElement());
console.log(mySet.getElement());
console.log(mySet.getElement());


/* Task 3 */

function makeMultiset() {
    let multiset = [];
    return {
        getElement: function() {
            return multiset[Math.floor(Math.random() * multiset.length)];
        },
        insertElement: function(e) {
            multiset.push(e);
        },
        deleteElement: function(e) {
            index = multiset.indexOf(e);
            if (index >= 0) multiset.splice(index, 1);
        },
        getMultiset: function() {
            return multiset;
        }
    }
}

let myMultiset = makeMultiset();
myMultiset.insertElement("a");
myMultiset.insertElement("b");
myMultiset.insertElement("c");
console.log(myMultiset.getMultiset());
myMultiset.insertElement("c");
console.log(myMultiset.getMultiset());
myMultiset.deleteElement("d");
console.log(myMultiset.getMultiset());
myMultiset.deleteElement("a");
console.log(myMultiset.getMultiset());
console.log(myMultiset.getElement());
console.log(myMultiset.getElement());
console.log(myMultiset.getElement());
<!doctype html>
<h1>WWW-Navigator</h1>
<style>
    textarea {
        margin: 1rem;
        display: block;
    }
    input {
        margin: 1rem;
    }
</style>
<form method="post">
    <fieldset>
        <legend>Wähle ein Thema, um einen Text zu schreiben.</legend>
        <select name="top_header">
            <option value="html">HTML</option>
            <option value="css">CSS</option>
            <option value="javascript">JavaScript</option>
        </select>
        <select name="sub_header">
        </select>
        <textarea name="content"></textarea>
        <input type="submit" value="Submit">
    </fieldset>
</form>
</html>

<?php
    $file = 'data/UE12A3_data.json';
    $contents = file_get_contents($file);
    $json = json_decode($contents, true);

    // save sent data
    if (isset($_POST['top_header']) && isset($_POST['sub_header']) && isset($_POST['content'])) {
        $top_header = $_POST['top_header'];
        $sub_header = $_POST['sub_header'];
        $content = $_POST['content'];

        echo $top_header . "<br>";
        echo $sub_header . "<br>";
        echo $content . "<br>";

        $json[$top_header][$sub_header] = $content;


        echo json_encode($json, true);



        if (file_put_contents($file, json_encode($json, true))) {
            echo "<script>alert('Inhalt erfolgreich gespeichert.')</script>";
        }
    }
?>

<script>
    let json = <?php echo json_encode($json) ?>;
    const top_header = document.querySelector('select[name="top_header"]');
    const sub_header = document.querySelector('select[name="sub_header"]');
    top_header.addEventListener('change', e => {
        Object.keys(json[e.target.value]).forEach(key => {
            const option = document.createElement('option');
            option.value = key;
            option.innerText = key;
            sub_header.append(option);
        })
    });
</script>
